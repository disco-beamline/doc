#!/usr/bin/env python
# coding: utf-8

# # Read images acquired with TELEMOS
# 
# How to process data 
# 
# Usefull reading before starting:
# - https://bioimagebook.github.io/index.html
# - https://scikit-image.org/skimage-tutorials/index.html#
# 

# ## With Python 

# The easiest way is to use the small help library developped at DISCO to load the image stack from micro-manager using the metadata of images.

# In[1]:


import sys
sys.path.append('../../DITB/')
from src.io.imread import mmread


# Import matplotlib and stackview libraries to display images

# In[2]:


import matplotlib.pyplot as plt
import stackview


# In[3]:


data_folder = '../../DITB/Examples/MACQ_multipos/'
images = mmread(data_folder)
images


# We see that the stack has:
# - 192 tiles
# - 1 z
# - 5 filters
# - 1 time
# - 512 pixels in x 
# - 512 pixels in y
# 
# Display the first tile of this stack using matplotlib

# In[4]:


_ = plt.imshow(images[0,0,0,0], cmap='gray')


# Use stackview.slide viewver to interactively inspect all tiles of a given filter

# In[5]:


stackview.slice(images[:,0,0,0,:,:])

