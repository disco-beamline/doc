# Welcome to the DISCO documentation for data-processing

This book is a test to write a clean documentation for users to help them to process their data obtain on the 
[DISCO Beamline of the Synchrotron-SOLEIL](https://www.synchrotron-soleil.fr/en/beamlines/disco).


```{warning}
This is an early version of the documentation and a lot of things are missing.
Please feel free to send us your suggestions to improve this documentation.
```

Main idea:

- Give user guide to download their data using globus
- Give user guide to work on their data:
  - Using the jupyter-notebook of grades via the remote.synchrotron-soleil.fr
  - Using their own jupyter notebook (or lab) installation
  - Using Their matlab 
  - Using ImageJ ?

