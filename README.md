![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---
Write documentation using Jupyter Book for DISCO Beamline data processing
---

The webversion is available at: https://discosoleil.gitlab.io/doc/
The principal repository of is located at : https://gitlab.synchrotron-soleil.fr/disco-beamline/doc
The **gitlab.com** is a mirror repo don't push merge request here use the above repository to push request, issues, etc...
